import telegram
import traceback
from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove, Update
from telegram.ext import (
    Updater,
    CommandHandler,
    ConversationHandler,
    MessageHandler,
    filters,
)
import numpy as np
import re


LOCANDINA, DATA_INIZIO, DATA_FINE, DATA_FINE_2, ORARIO_INIZIO, ORARIO_FINE, ORARIO_FINE_2, LOCATION, TITOLO, DESCRIZIONE = range(10)

TOKEN = "5535922897:AAG8ovXwpghC1MsPqF_KzZXQTR_pYbWCGlQ"


evento = np.empty((1, 8), dtype=object)


def start(update, context) -> int:
    """Starts the conversation and asks for picture of event"""
    update.message.reply_text(
        "Ciao! Sono il bot di Apuliacore, per iniziare invia la locandina dell'evento: "
        "Invia /cancel in qualsiasi momento per smettere di parlare con me\n\n"
        )

    return LOCANDINA


def locandina(update, context) -> int:
    """Stores the photo and asks for a date."""
    user = update.message.from_user
    photo_file = update.message.photo[-1].file_id
    evento[0, 0] = photo_file
    context.user_data['locandina'] = photo_file
    update.message.reply_text(
        "Perfetto! Ora inserisci la data di inizio evento [in formato gg.mm.aaaa]"
    )

    return DATA_INIZIO
    

def data_inizio(update, context) -> int:
    """Stores date and asks if ending date."""
    reply_keyboard = [["Sì", "No"]]
    user = update.message.from_user
    context.user_data['data_inizio'] = re.escape(update.message.text)
    update.message.reply_text("Grazie! L'evento ha una data di fine diversa da quella di inizio?", 
        reply_markup=ReplyKeyboardMarkup(
            reply_keyboard, one_time_keyboard=True, input_field_placeholder="Sì o no?"),
    )

    return DATA_FINE


def data_fine(update, context) -> int:
    """Asks for ending date."""
    user = update.message.from_user
    update.message.reply_text("Inserisci la data di fine evento:")

    return DATA_FINE_2

def data_fine_2(update, context) -> int:
    """Stores ending date, asks for time"""
    user = update.message.from_user
    context.user_data['data_fine'] = re.escape("- " + update.message.text)
    evento[0, 6] = re.escape("- " + update.message.text)
    update.message.reply_text("A che ora inizia l'evento?[formato hh:mm]")

    return ORARIO_INIZIO


def skip_data_fine(update, context) -> int:
    evento[0, 6] = ""
    context.user_data['data_fine'] = ""
    update.message.reply_text("A che ora inizia l'evento?[formato hh:mm]")

    return ORARIO_INIZIO


def orario_inizio(update, context) -> int:
    """Stores Asks if end time"""
    evento[0, 2] = re.escape(update.message.text)
    context.user_data['orario_inizio'] = re.escape(update.message.text)
    reply_keyboard = [["Sì", "No"]]
    update.message.reply_text("Vuoi inserire l'orario di fine evento?", 
        reply_markup=ReplyKeyboardMarkup(
            reply_keyboard, one_time_keyboard=True, input_field_placeholder="Sì o no?"),
    )
    return ORARIO_FINE

def skip_orario_fine(update, context) -> int:
    """Skips end time asks for location"""
    evento[0, 7] = ""
    context.user_data['orario_fine'] = ""
    update.message.reply_text("Ora inserisci il luogo dell'evento: [ad esempio: Teatro Petruzzelli, Bari]")
    return LOCATION


def orario_fine(update, content) -> int:
    """Asks end time"""
    update.message.reply_text("A che ora finisce l'evento? [formato hh:mm]")
    return ORARIO_FINE_2
    

def orario_fine_2(update, context) -> int:
    """stores end time and asks location"""
    evento[0, 7] = re.escape("- " + update.message.text)
    context.user_data['orario_fine'] = re.escape("- " + update.message.text)
    update.message.reply_text("Ora inserisci il luogo dell'evento: [ad esempio: Teatro Petruzzelli, Bari]")
    return LOCATION


def location(update, context) -> int:
    """Asks for title of event"""
    evento[0, 3] = re.escape(update.message.text)
    context.user_data['location'] = re.escape(update.message.text)
    update.message.reply_text("Come si chiama l'evento?")
    return TITOLO


def titolo(update, context) -> int:
    """Asks for description of event"""
    evento[0, 4] = re.escape(update.message.text)
    context.user_data['titolo'] = re.escape(update.message.text)
    update.message.reply_text("Inserisci le info utili per l'evento (come link per acquistare biglietti, programmi, line-up, etc):")
    return DESCRIZIONE


def descrizione(update, context) -> int:
    context.user_data['descrizione'] = re.escape(update.message.text).replace("!", "\!")
    username = update.message.from_user.username
    primonome = update.message.from_user.first_name
    try: 
        telegram.Bot(token=TOKEN).send_photo(
            chat_id=-1001633714586, 
            photo=context.user_data['locandina'],
            caption=(f"📅{context.user_data['data_inizio']} {context.user_data['data_fine']} \| {context.user_data['orario_inizio']} {context.user_data['orario_fine']}\n📍{context.user_data['location']}\n\n`{context.user_data['titolo']}`\n\n{context.user_data['descrizione']}"),
            parse_mode=telegram.ParseMode.MARKDOWN_V2
        )
        telegram.Bot(token=TOKEN).sendMessage(chat_id=-1001633714586,text=f"inviato da {primonome}, @{username}")
        update.message.reply_text("L'evento è stato inviato ai moderatori! Grazie per la richiesta")
    except telegram.error.BadRequest:
        update.message.reply_text("Si è verificato un errore con la generazione del messaggio :( \n Prova a cambiare leggermente la descrizione o il titolo o contatta il creatore del bot @luigijs per assistenza")
        telegram.Bot(token=TOKEN).sendMessage(chat_id=-1001633714586,text=f"{primonome}, @{username} ha provato ad inviare un evento ma si è verificato un errore: {traceback.format_exc()}")
    
    return ConversationHandler.END


def cancel(update, context) -> int:
    """Cancels and ends the conversation."""
    update.message.reply_text("Evento annullato!")
    return ConversationHandler.END


def main():
    
    upd = Updater(TOKEN, use_context=True)
    disp = upd.dispatcher
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler("start", start)],
        states={
            LOCANDINA: [MessageHandler(filters.Filters.photo, locandina)],
            DATA_INIZIO: [MessageHandler(filters.Filters.regex("((\d{2}|\d{1})(/|\.)(\d{2}|\d{1})(/|\.)\d{4})"), data_inizio)],
            DATA_FINE: [MessageHandler(filters.Filters.regex("Sì"), data_fine), MessageHandler(filters.Filters.regex("No"), skip_data_fine)],
            DATA_FINE_2: [MessageHandler(filters.Filters.regex("((\d{2}|\d{1})(/|\.)(\d{2}|\d{1})(/|\.)\d{4})"), data_fine_2)],
            ORARIO_INIZIO: [MessageHandler(filters.Filters.regex("\d+:\d+"), orario_inizio)],
            ORARIO_FINE: [MessageHandler(filters.Filters.regex("Sì"), orario_fine), MessageHandler(filters.Filters.regex("No"), skip_orario_fine)],
            ORARIO_FINE_2: [MessageHandler(filters.Filters.regex("\d+:\d+"), orario_fine_2)],
            LOCATION: [
                MessageHandler(filters.Filters.text & (~ filters.Filters.command), location)
            ],
            TITOLO: [MessageHandler(filters.Filters.text & (~ filters.Filters.command), titolo)],
            DESCRIZIONE: [MessageHandler(filters.Filters.text & (~ filters.Filters.command), descrizione)],
        },
        fallbacks=[CommandHandler("cancel", cancel)],
    )
    disp.add_handler(conv_handler)
    upd.start_polling()
    upd.idle()


if __name__ == '__main__':
    main()
