import re
import numpy as np
from datetime import time, timedelta, datetime, date
from telegram.ext import Updater, MessageHandler, Filters
from google.cloud import bigquery
from google.oauth2 import service_account
from googleapiclient.discovery import build

credentials = service_account.Credentials.from_service_account_file(
   "/root/apuliacore-334511-249b6df1b8a0.json", scopes=["https://www.googleapis.com/auth/calendar"],
)
service = build('calendar', 'v3', credentials=credentials)

client = bigquery.Client(credentials=credentials, project=credentials.project_id,)
TOKEN = '****'#token segreta


def handle_all_messages(update, context):
    evento = np.empty((1, 11), dtype=object)
    try:
        event_message = update.channel_post.caption
    except AttributeError:
        event_message = update.channel_post.text
    string = str(event_message)
    dd_search = re.search("((\d{2}|\d{1})(/|\.)(\d{2}|\d{1})(/|\.)\d{4})", string)
    dd_search_other = re.search("((\d{2}|\d{1})(/|\.)(\d{2}|\d{1})(/|\.)\d{2})", string)
    if dd_search:
        data_inizio = re.findall(r'\d+', dd_search.group(0))
    elif dd_search_other:
        data_inizio = re.findall(r'\d+', dd_search_other.group(0))
        data_inizio[2] = "20{}".format(data_inizio[2])
        print(data_inizio)
    print(dd_search)
    print(dd_search_other)
    data_fine_search = re.search(
        "(-|—|\.\.\.) ((\d{2}|\d{1})(/|\.)(\d{2}|\d{1})(/|\.)\d{4})", string)
    location_search = re.search("📍.*", string)
    titolo_search = re.search("(📍.*)\n\n((.|\r?\n(?!\r?\n))*)\n\n(.*)\n", string)
    print("messaggio ricevuto!")
    if update.effective_message.chat.id == -1001680325962:
        print("canale esatto!")
        if dd_search or dd_search_other:
            print("pattern trovato!")
            for j in range(3):
                evento[0, j] = data_inizio[j]
            orario_inizio_search = re.search("\d+:\d+", string)
            if location_search:
                location_tagliata = location_search.group(0).split('\\')[0]
                location_fixed = location_tagliata[1:]
                evento[0, 6] = location_fixed
            if titolo_search:
                evento[0, 3] = titolo_search.group(2)
                evento[0, 10] = update.channel_post.link
            if orario_inizio_search:
                evento[0, 4] = orario_inizio_search.group(0)
                orario_fine_search = re.search("(- \d+:\d+)|(— \d+:\d+)", string)
                print("orario fine trovato!")
                if orario_fine_search:
                    evento[0, 5] = orario_fine_search.group(0)[2:]
                    for j in range(3):
                        evento[0, j + 7] = data_inizio[j]
                else:
                    print("orario fine non trovato!")
                    ora_inizio = int((re.search("\d+", orario_inizio_search.group(0))).group(0))
                    time_fine_standard = datetime.combine(date(year=int(evento[0, 2]), month=int(evento[0, 1]),
                                                               day=int(evento[0, 0])), time(ora_inizio, 00)) + timedelta(hours=2)
                    evento[0, 5] = "{}:00".format(time_fine_standard.time().hour)
                    evento[0, 7] = str(time_fine_standard.date().day)
                    evento[0, 8] = str(time_fine_standard.date().month)
                    evento[0, 9] = str(time_fine_standard.date().year)
            if data_fine_search:
                print("data fine trovata!")
                data_fine = re.findall(r'\d+', data_fine_search.group(0))
                for j in range(3):
                    evento[0, j + 7] = data_fine[j]

        if evento[0, 0] is not None:
            print(evento)
            if evento[0, 4] is not None:
                event = {
                    'summary': '{}'.format(evento[0, 3]),
                    'location': '{}'.format(evento[0, 6]),
                    'description': '{}'.format(evento[0, 10]),
                    'start': {
                        'dateTime': '{}-{}-{}T{}:00+01:00'.format(evento[0, 2], evento[0, 1], evento[0, 0],
                                                                  evento[0, 4]),
                        'timeZone': 'Europe/Rome',
                    },
                    'end': {
                        'dateTime': '{}-{}-{}T{}:00+01:00'.format(evento[0, 9], evento[0, 8], evento[0, 7], evento[0, 5]),
                        'timeZone': 'Europe/Rome',
                    }}
                event = service.events().insert(calendarId='dp2igoio44dihlpcrmi81jatho@group.calendar.google.com',
                                                body=event).execute()
                print('Event created: %s' % (event.get('htmlLink')))
            else:
                event = {
                    'summary': '{}'.format(evento[0, 3]),
                    'location': '{}'.format(evento[0, 6]),
                    'description': '{}'.format(evento[0, 10]),
                    'start': {
                        'date': '{}-{}-{}'.format(evento[0, 2], evento[0, 1], evento[0, 0])
                    },
                    'end': {
                        'date': '{}-{}-{}'.format(evento[0, 9], evento[0, 8], evento[0, 7])
                    }}
                event = service.events().insert(calendarId='dp2igoio44dihlpcrmi81jatho@group.calendar.google.com',
                                                body=event).execute()
                print('Event created: %s' % (event.get('htmlLink')))
            print('Evento caricato!')


def main():
    upd = Updater(TOKEN, use_context=True)
    disp = upd.dispatcher
    disp.add_handler(MessageHandler(Filters.regex(re.compile(r'.*', re.IGNORECASE)), handle_all_messages))
    disp.add_handler(MessageHandler(Filters.caption_regex(re.compile(r'.*', re.IGNORECASE)), handle_all_messages))
    upd.start_polling()
    upd.idle()


if __name__ == '__main__':
    main()
    
