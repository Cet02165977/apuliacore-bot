let {google} = require( 'googleapis');

let { readFileSync } = require('fs');
let rawdata = readFileSync('apuliacore-334511-be1faafc1cc0.json');
let privatekey = JSON.parse(rawdata);

// configure a JWT auth client
let jwtClient = new google.auth.JWT(
  privatekey.client_email,
  null,
  privatekey.private_key,
  ['https://www.googleapis.com/auth/calendar.readonly']);
//authenticate request
jwtClient.authorize(function (err, tokens) {
if (err) {
console.log(err);
return;
} else {
console.log("Successfully connected!");
}
});

var today = new Date();
today.setUTCHours(0,0,0,0);
var giornooggi = new Date(today);
giornooggi.setDate(today.getDate() - 1);
var giornodomani = new Date(today);
giornodomani.setDate(today.getDate() - 1);
var tomorrow = new Date(today);
var aftertomorrow = new Date(today);
tomorrow.setDate(tomorrow.getDate() + 1);
aftertomorrow.setDate(aftertomorrow.getDate() + 2);
var calendar = google.calendar({version: 'v3'});



let TelegramBot = require('node-telegram-bot-api');
let moment = require('moment');
let nominatim = require('nominatim-geocoder');
let orario_iniziooggi = [];
let orario_iniziodomani = [];
let nome_luogo = [];
let eventstoday = [];
let eventstomorrow = [];


const StaticMaps = require('staticmaps');
const { exit } = require('process');
const options = {
    width: 2000,
    height: 1600
  };
const zoom = 9;
const center = [17, 41];
const marker = {
    img: `/root/marker.png`,
    width: 50,
    height: 50,
    offsetY: 100,
    offsetX: 50
  };
const text = {
    size: 30,
    width: 1,
    fill: '#ffffff',
    color: '#ffffff',
    font: 'Calibri',
    anchor: 'middle',
    offsetY: 12
  };
const token = '5038939643:AAGlAGcjbkRiWIpppycnYFPRfeaG3Jpmxu0';
const bot = new TelegramBot(token, {polling: true});


bot.on('message', async (msg) => {
    let today = new Date();
    if (msg.text == null){
        return
    }
    var comandostart = "/start";
    if (msg.text.toString().indexOf(comandostart) === 0){
        bot.sendMessage(msg.chat.id, "Benvenuto/a sul bot di Apuliacore! premi / per scoprire le funzioni del bot")};
    var comandoeventi = "/eventi";
    if (msg.text.toString().indexOf(comandoeventi) === 0){
        bot.sendMessage(msg.chat.id, "Per favore scegli il giorno di cui vuoi conoscere gli eventi:", {
            "reply_markup": {"keyboard": [["Oggi", "Domani"]], "one_time_keyboard": true},
        }
    );
        console.log(today.getDate());
        console.log(giornooggi.getDate());
        console.log(giornodomani.getDate());
        
        async function funzioneeventi(){
            bot.on('text', async (msg3) => {
                var oggi = "Oggi";
                if (msg3.text.toString().indexOf(oggi) === 0) {
                    if (msg3.text == null){return};
                    if (msg.text == null){return};
                    if (giornooggi.getDate() != today.getDate()){
                        bot.sendMessage(msg.chat.id, "Attendi mentre si caricano i risultati e la mappa (potrebbe metterci un po') ...");
                        console.log("funzione chiamata!");
                        calendar.events.list({
                            auth: jwtClient,
                            calendarId:'dp2igoio44dihlpcrmi81jatho@group.calendar.google.com',
                            timeMin: (today).toISOString(),
                            timeMax: (tomorrow).toISOString(),
                            timeZone: "Europe/Rome",
                            singleEvents: true,
                            orderBy: 'startTime',
                            }, (err, res) =>  
                        {
                        if (err) return console.log('The calendar API returned an error: ' + err);
                        if (msg3.text == null){return};
                        console.log("eventi analizzati!");
                        console.log(tomorrow);
			console.log(today);
			console.log(aftertomorrow);
                        let eventstoday = res.data.items;
                        async function eventidioggi(){
                            map = new StaticMaps(options);
                            for (var i = 0; i < eventstoday.length; i++){
                                nome_luogo[i] = await new nominatim().search( {q: eventstoday[i].location});
                            }
                            eventstoday.map((event, i) => {
                                const start = event.start.dateTime || event.start.date;
                                orario_iniziooggi[i] = moment.utc(start).add(1, "hours").format("HH:mm");
                            });
                            for (var i = 0; i < eventstoday.length; i++){
                                if (nome_luogo[i].length != 0) {
                                    console.log(nome_luogo[i]);
                                    console.log(nome_luogo[i][0].lon);
                                    console.log(nome_luogo[i][0].lat);
                                    coordinate1 = parseFloat(nome_luogo[i][0].lon);
                                    coordinate2 = parseFloat(nome_luogo[i][0].lat);
                                    await bot.sendMessage(msg.chat.id,`${i + 1}: ${orario_iniziooggi[i]} - ${eventstoday[i].location} \n ${eventstoday[i].summary}`);
                                    marker.coord = [coordinate1, coordinate2];
                                    map.addMarker(marker);
                                    text.coord = [coordinate1, coordinate2];
                                    text.text = `${i + 1}`
                                    map.addText(text);
                                }
                                    else{
                                    await bot.sendMessage(msg.chat.id, `${i + 1}: Nessuna coordinata trovata per questo luogo: ${eventstoday[i].location}`);
                            }};
                            bot.sendMessage(msg.chat.id, "Attendi la renderizzazione della mappa...");
                            await map.render(center, zoom);
                            await map.image.save('mappa1.png');
                            console.log("mappacreata!");
                            bot.sendPhoto(msg.chat.id, "mappa1.png");
                            msg.text = null;
                            msg3.text = null;
                            return;
                            };
                        eventidioggi();
                        today = new Date();
                        today.setUTCHours(0,0,0,0);
                        tomorrow.setDate(today.getDate() + 1);
                        aftertomorrow.setDate(today.getDate() + 2);
                        giornooggi = today;
                        console.log(today);console.log(tomorrow); console.log(giornooggi);
                        return;
                    })};
                    if (giornooggi.getDate() == today.getDate()){
                        calendar.events.list({
                            auth: jwtClient,
                            calendarId:'dp2igoio44dihlpcrmi81jatho@group.calendar.google.com',
                            timeMin: (today).toISOString(),
                            timeMax: (tomorrow).toISOString(),
                            timeZone: "Europe/Rome",
                            singleEvents: true,
                            orderBy: 'startTime',
                            }, (err, res) =>  {
                                if (err) return console.log('The calendar API returned an error: ' + err);
                                if (msg3.text == null){return};
                                console.log("eventi analizzati!");
                                let eventstoday = res.data.items;
                                eventstoday.map((event, i) => {
                                    const start = event.start.dateTime || event.start.date;
                                    orario_iniziooggi[i] = moment.utc(start).add(1, "hours").format("HH:mm");
                                });
                                console.log("eventi di oggi ottenuti!");
                                async function eventiripetutioggi(){
                                    for (var i = 0; i < eventstoday.length; i++){
                                        console.log("sei arrivato qui! finale");
                                        await bot.sendMessage(msg.chat.id,`${i + 1}: ${orario_iniziooggi[i]} - ${eventstoday[i].location} \n ${eventstoday[i].summary}`)};
                                        bot.sendPhoto(msg.chat.id, "mappa1.png");
                                    }
                                console.log("sei arrivato qui!");
                                eventiripetutioggi();
                                msg.text = null;
                                msg3.text = null;
                                console.log("sei arrivato qui! 1");
                                return;
                        })
                        
                        console.log("sei arrivato qui! 2");
                        return;
                    }
                    console.log("sei arrivato qui! 3");
                    return;
                };
                var domani = "Domani";
                if (msg3.text.toString().indexOf(domani) === 0) {
                    if (msg3.text == null){return};
                    if (msg.text == null){return};
                    if (giornodomani.getDate() != today.getDate()){
                        bot.sendMessage(msg.chat.id, "Attendi mentre si caricano i risultati e la mappa (potrebbe metterci un po') ...");
                        console.log("funzione chiamata!");
                        calendar.events.list({
                        auth: jwtClient,
                        calendarId:'dp2igoio44dihlpcrmi81jatho@group.calendar.google.com',
                        timeMin: (tomorrow).toISOString(),
                        timeMax: (aftertomorrow).toISOString(),
                        timeZone: "Europe/Rome",
                        singleEvents: true,
                        orderBy: 'startTime',
                        }, (err, res) =>  
                        {
                        if (err) return console.log('The calendar API returned an error: ' + err);
                        if (msg3.text == null){return};
                        console.log("eventi analizzati!");
                        let eventstomorrow = res.data.items;
                        async function eventididomani(){
                            map = new StaticMaps(options);
                            for (var i = 0; i < eventstomorrow.length; i++){
                                nome_luogo[i] = await new nominatim().search( {q: eventstomorrow[i].location});
                            }
                            eventstomorrow.map((event, i) => {
                                const start = event.start.dateTime || event.start.date;
                                orario_iniziodomani[i] = moment.utc(start).add(1, "hours").format("HH:mm");
                            });
                            for (var i = 0; i < eventstomorrow.length; i++){
                                if (nome_luogo[i].length != 0) {
                                    console.log(nome_luogo[i]);
                                    console.log(nome_luogo[i][0].lon);
                                    console.log(nome_luogo[i][0].lat);
                                    coordinate1 = parseFloat(nome_luogo[i][0].lon);
                                    coordinate2 = parseFloat(nome_luogo[i][0].lat);
                                    await bot.sendMessage(msg.chat.id,`${i + 1}: ${orario_iniziodomani[i]} - ${eventstomorrow[i].location} \n ${eventstomorrow[i].summary}`);
                                    marker.coord = [coordinate1, coordinate2];
                                    map.addMarker(marker);
                                    text.coord = [coordinate1, coordinate2];
                                    text.text = `${i + 1}`
                                    map.addText(text);
                                }
                                    else{
                                    await bot.sendMessage(msg.chat.id, `${i + 1}: Nessuna coordinata trovata per questo luogo: ${eventstomorrow[i].location}`);
                            }};
                            bot.sendMessage(msg.chat.id, "Attendi la renderizzazione della mappa...");
                            await map.render(center, zoom);
                            await map.image.save('mappa2.png');
                            console.log("mappacreata!");
                            bot.sendPhoto(msg.chat.id, "mappa2.png");
                            msg.text = null;
                            msg3.text = null;
                            return;               
                        };
                        eventididomani();
                        var today = new Date();
                        today.setUTCHours(0,0,0,0);
                        tomorrow.setDate(today.getDate() + 1);
                        aftertomorrow.setDate(today.getDate() + 2);
                        giornodomani = today;
                        return;
                    })};
                    if (giornodomani.getDate() == today.getDate()){
                        calendar.events.list({
                            auth: jwtClient,
                            calendarId:'dp2igoio44dihlpcrmi81jatho@group.calendar.google.com',
                            timeMin: (tomorrow).toISOString(),
                            timeMax: (aftertomorrow).toISOString(),
                            timeZone: "Europe/Rome",
                            singleEvents: true,
                            orderBy: 'startTime',
                            }, (err, res) =>  {
                                if (err) return console.log('The calendar API returned an error: ' + err);
                                if (msg3.text == null){return};
                                console.log("eventi analizzati!");
                                let eventstomorrow = res.data.items;
                                eventstomorrow.map((event, i) => {
                                    const start = event.start.dateTime || event.start.date;
                                    orario_iniziodomani[i] = moment.utc(start).add(1, "hours").format("HH:mm");
                                });
                                console.log("eventi di domani ottenuti!");
                                async function eventiripetutidomani(){
                                for (var i = 0; i < eventstomorrow.length; i++){
                                    console.log("sei arrivato qui! finale");
                                    await bot.sendMessage(msg.chat.id,`${i + 1}: ${orario_iniziodomani[i]} - ${eventstomorrow[i].location} \n ${eventstomorrow[i].summary}`)};
                                    bot.sendPhoto(msg.chat.id, "mappa2.png");
                                }
                                eventiripetutidomani();
                                msg.text = null;
                                msg3.text = null;
                            })
                        
                        return;
                    }
                return;
                };
            })
        };
        funzioneeventi();   
    }      

    var segnala = "/segnala"
    if (msg.text.toString().indexOf(segnala) === 0){
        if (msg.text == null){return};
        bot.sendMessage(msg.chat.id, "Condividi la locandina di un evento che vorresti segnalare");
        function funzionelocandine(){
        bot.on('photo', (msg1) => {
            if (msg1.photo == null){return};
            if (msg.text == null){return};
            console.log(msg1.photo);
            bot.sendMessage(msg1.chat.id, "vuoi inviare questa foto come locandina agli admin di apuliacore? NB: il tuo ID viene salvato nel momento in cui invii un'immagine", 
            {"reply_markup": {"keyboard": [["Sì", "No"]], "one_time_keyboard": true}});
            
            bot.on('text', (msg2) => {
                if (msg2.text.toString().indexOf("Sì") === 0){
                    if (msg1.photo == null){return};
                    var fileId = msg1.photo[msg1.photo.length - 1].file_id;
                    var personId = msg1.chat.username;
                    bot.sendPhoto(-662273223, fileId, {caption: personId, parse_mode: 'Markdown'});
                    bot.sendMessage(msg1.chat.id, "Locandina inviata!");
                    msg1.photo = null;
                    msg.text = null;
                    return;
                }
                if (msg2.text.toString().indexOf("No") === 0){
                    if (msg1.photo == null){return};
                    bot.sendMessage(msg1.chat.id, "Locandina non inviata!");
                    msg1.photo = null;
		    msg.text = null;
                    return;
                }
            return;
            })
            return;
        })
        return;}
        funzionelocandine();
        }
});

