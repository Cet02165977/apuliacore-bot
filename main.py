from Google import Create_Service
from telethon import TelegramClient
from telethon.tl.types import InputPeerChat
import sys
import re
import numpy as np
from datetime import time, timedelta, datetime, date

CLIENT_SECRET_FILE = 'C:/Users/swamp/Downloads/client_secret_apuliacore.json'
API_NAME = 'calendar'
API_VERSION = 'v3'
SCOPES = ['https://www.googleapis.com/auth/calendar']
chat_id = -1001680325962
api_id = 17585550
api_hash = '9fc023c78976161227d9f7be063efd3e'

service = Create_Service(CLIENT_SECRET_FILE, API_NAME, API_VERSION, SCOPES)

client = TelegramClient('session_id', api_id=api_id, api_hash=api_hash)
chat = InputPeerChat(chat_id)
np.set_printoptions(threshold=sys.maxsize)


async def main():
    event_messages = await client.get_messages(chat_id, None)
    evento = np.empty((len(event_messages), 11), dtype=object)
    for i in range(len(event_messages)):
        string = str(event_messages[i].message)
        dd_search = re.search("((\d{2}|\d{1})\.(\d{2}|\d{1})\.\d{4})|((\d{2}|\d{1})/(\d{2}|\d{1})/\d{4})", string)
        data_fine_search = re.search("(-|—|\.\.\.) ((\d{2}|\d{1})\.(\d{2}|\d{1})\.\d{4})|((\d{2}|\d{1})/(\d{2}|\d{1})/\d{4})", string)
        location_search = re.search("📍.*", string)
        titolo_search = re.search("(📍.*)\n\n((.|\r?\n(?!\r?\n))*)\n\n(.*)\n", string)
        if dd_search:
            data_inizio = re.findall(r'\d+', dd_search.group(0))
            for j in range(3):
                evento[i, j] = data_inizio[j]
            orario_inizio_search = re.search("\d+:\d+", string)
            if orario_inizio_search:
                evento[i, 4] = orario_inizio_search.group(0)
                orario_fine_search = re.search("(- \d+:\d+)|(— \d+:\d+)", string)

                if orario_fine_search:
                    evento[i, 5] = orario_fine_search.group(0)[2:]
                else:
                    ora_inizio = int((re.search("\d+", orario_inizio_search.group(0))).group(0))
                    time_fine_standard = datetime.combine(date.today(), time(ora_inizio, 00)) + timedelta(hours=2)
                    evento[i, 5] = "{}:00".format(time_fine_standard.time().hour)
            else:
                evento[i, 4] = "00:00"
            if location_search:
                location_tagliata = location_search.group(0).split('\\')[0]
                location_fixed = location_tagliata[1:]
                evento[i, 6] = location_fixed
            if titolo_search:
                evento[i, 3] = titolo_search.group(2)
                evento[i, 10] = titolo_search.group(4)
            if data_fine_search:
                data_fine = re.findall(r'\d+', data_fine_search.group(0))
                for j in range(3):
                    evento[i, j + 7] = data_fine[j]
            else:
                for j in range(3):
                    evento[i, j + 7] = data_inizio[j]

    print(evento)


    for i in range(len(event_messages)):
        if evento[i, 0] is not None:
            if evento[i, 4] is not None:
                event = {
                    'summary': '{}'.format(evento[i, 3]),
                    'location': '{}'.format(evento[i, 6]),
                    'description': '{}'.format(evento[i, 10]),
                    'start': {
                        'dateTime': '{}-{}-{}T{}:00+01:00'.format(evento[i, 2], evento[i, 1], evento[i, 0], evento[i, 4]),
                        'timeZone': 'Europe/Rome',
                    },
                    'end': {
                        'dateTime': '{}-{}-{}T23:00:00+01:00'.format(evento[i, 9], evento[i, 8], evento[i, 7]),
                        'timeZone': 'Europe/Rome',
                    }}
                event = service.events().insert(calendarId='dp2igoio44dihlpcrmi81jatho@group.calendar.google.com', body=event).execute()
                print('Event created: %s' % (event.get('htmlLink')))
            else:
                event = {
                    'summary': '{}'.format(evento[i, 3]),
                    'location': '{}'.format(evento[i, 6]),
                    'description': '{}'.format(evento[i, 10]),
                    'start': {
                        'dateTime': '{}-{}-{}'.format(evento[i, 2], evento[i, 1], evento[i, 0],
                                                                  evento[i, 4]),
                        'timeZone': 'Europe/Rome',
                    },
                    'end': {
                        'dateTime': '{}-{}-{}'.format(evento[i, 9], evento[i, 8], evento[i, 7]),
                        'timeZone': 'Europe/Rome',
                    }}
                event = service.events().insert(calendarId='dp2igoio44dihlpcrmi81jatho@group.calendar.google.com',
                                                body=event).execute()
                print('Event created: %s' % (event.get('htmlLink')))
            print('Evento {} caricato!'.format(i))




with client:
    client.loop.run_until_complete(main())
